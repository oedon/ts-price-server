export const appl = {
    name: 'Apple',
    url: 'https://finnhub.io/api/v1/quote?symbol=AAPL&token=c9ttobiad3i9vd5j9q7g',
    ids: { sym: 'APPL', ch24: 'APPL24' },
};
export const amd = {
    name: 'AMD',
    url: 'https://finnhub.io/api/v1/quote?symbol=AMD&token=c9ttobiad3i9vd5j9q7g',
    ids: { sym: 'AMD', ch24: 'AMD24' },
};
export const pypl = {
    name: 'PayPal',
    url: 'https://finnhub.io/api/v1/quote?symbol=PYPL&token=c9ttobiad3i9vd5j9q7g',
    ids: { sym: 'PYPL', ch24: 'PYPL24' },
};
export const nflx = {
    name: 'Netflix',
    url: 'https://finnhub.io/api/v1/quote?symbol=NFLX&token=c9ttobiad3i9vd5j9q7g',
    ids: { sym: 'NFLX', ch24: 'NFLX24' },
};
export const nvda = {
    name: 'Nvidia',
    url: 'https://finnhub.io/api/v1/quote?symbol=NVDA&token=c9ttobiad3i9vd5j9q7g',
    ids: { sym: 'NVDA', ch24: 'NVDA24' },
};
export const tsla = {
    name: 'Tesla',
    url: 'https://finnhub.io/api/v1/quote?symbol=TSLA&token=c9ttobiad3i9vd5j9q7g',
    ids: { sym: 'TSLA', ch24: 'TSLA24' },
};
