var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import * as coin from './coin.js';
import * as stonk from './stonk.js';
import * as call from './call.js';
const CoinLoopDaddy = () => __awaiter(void 0, void 0, void 0, function* () {
    yield call.getCoinPrice(coin.ada);
    yield call.getCoinPrice(coin.btc);
    yield call.getCoinPrice(coin.eth);
    yield call.getCoinPrice(coin.xmr);
    setTimeout(CoinLoopDaddy, 30000);
});
const StonkLoopDaddy = () => __awaiter(void 0, void 0, void 0, function* () {
    yield call.getStonkPrice(stonk.appl);
    yield call.getStonkPrice(stonk.amd);
    yield call.getStonkPrice(stonk.pypl);
    yield call.getStonkPrice(stonk.nflx);
    yield call.getStonkPrice(stonk.nvda);
    yield call.getStonkPrice(stonk.tsla);
    setTimeout(StonkLoopDaddy, 60000);
});
CoinLoopDaddy();
StonkLoopDaddy();
