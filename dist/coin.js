export const ada = {
    name: 'cardano',
    url: 'https://api.coincap.io/v2/assets/cardano',
    ids: { sym: 'ada', ch24: 'ada24' },
};
export const btc = {
    name: 'bitcoin',
    url: 'https://api.coincap.io/v2/assets/bitcoin',
    ids: { sym: 'btc', ch24: 'btc24' },
};
export const eth = {
    name: 'ethereum',
    url: 'https://api.coincap.io/v2/assets/ethereum',
    ids: { sym: 'eth', ch24: 'eth24' },
};
export const xmr = {
    name: 'monero',
    url: 'https://api.coincap.io/v2/assets/monero',
    ids: { sym: 'xmr', ch24: 'xmr24' },
};
