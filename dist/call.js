var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
/** Get HTML ID */
export function getId(htmlID) {
    const id = document.getElementById(htmlID);
    return id;
}
/** Get Coin Price from CoinCap */
export const getCoinPrice = (obj) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        // Await response
        const response = yield fetch(obj.url);
        const json = yield response.json();
        // Parse json to price and change
        const price = Number(json.data.priceUsd);
        const change = Number(json.data.changePercent24Hr);
        // Get HTML IDs
        const idSym = getId(obj.ids.sym);
        const idCh24 = getId(obj.ids.ch24);
        // Set Price
        idSym.innerHTML = price.toFixed(4) + " " + "USD";
        // Set Change and css color
        if (change < 0) {
            idCh24.style.color = "red";
            idCh24.innerHTML = change.toFixed(2) + " " + "%";
        }
        if (change >= 0) {
            idCh24.style.color = "green";
            idCh24.innerHTML = change.toFixed(2) + " " + "%";
        }
        // Return Promise
        return json;
    }
    catch (err) {
        console.error(err);
    }
});
/** Get Stonk Price */
export const getStonkPrice = (obj) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        // Await response
        const response = yield fetch(obj.url);
        const json = yield response.json();
        // Parse json to price and change
        const price = json.c;
        const change = json.dp;
        // Get HTML IDs
        const idSym = getId(obj.ids.sym);
        const idCh24 = getId(obj.ids.ch24);
        // Set Price
        idSym.innerHTML = price.toFixed(4) + " " + "USD";
        // Set Change and css color
        if (change < 0) {
            idCh24.style.color = "red";
            idCh24.innerHTML = change.toFixed(2) + " " + "%";
        }
        if (change >= 0) {
            idCh24.style.color = "green";
            idCh24.innerHTML = change.toFixed(2) + " " + "%";
        }
        // Return Promise
        return json;
    }
    catch (err) {
        console.error(err);
    }
});
