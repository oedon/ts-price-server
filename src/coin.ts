import * as call from './call.js'

export interface CoinInterface {
  data: {
    id: string
    rank: string
    symbol: string
    name: string
    supply: string
    maxSupply: string
    marketCapUsd: string
    volumeUsd24Hr: string
    priceUsd: string
    changePercent24Hr: string
    vwap24Hr: string
    explorer: string
  }
}

export const ada: call.SymbolInterface = {
  name: 'cardano',
  url: 'https://api.coincap.io/v2/assets/cardano',
  ids: { sym: 'ada', ch24: 'ada24' },
}

export const btc: call.SymbolInterface = {
  name: 'bitcoin',
  url: 'https://api.coincap.io/v2/assets/bitcoin',
  ids: { sym: 'btc', ch24: 'btc24' },
}

export const eth: call.SymbolInterface = {
  name: 'ethereum',
  url: 'https://api.coincap.io/v2/assets/ethereum',
  ids: { sym: 'eth', ch24: 'eth24' },
}

export const xmr: call.SymbolInterface = {
  name: 'monero',
  url: 'https://api.coincap.io/v2/assets/monero',
  ids: { sym: 'xmr', ch24: 'xmr24' },
}
