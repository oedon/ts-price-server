import * as coin from './coin.js'
import * as stonk from './stonk.js'

/** Get HTML ID */
export function getId(htmlID: string): HTMLInputElement {
    const id: HTMLInputElement = document.getElementById(
        htmlID
    ) as HTMLInputElement
    return id
}

/** Symbol Interface */
export interface SymbolInterface {
    name: string
    url: string
    ids: { sym: string; ch24: string }
}

/** Get Coin Price from CoinCap */
export const getCoinPrice = async (obj: SymbolInterface): Promise<coin.CoinInterface> => {
    try {
        // Await response
        const response: Response = await fetch(obj.url)
        const json: coin.CoinInterface = await response.json()

        // Parse json to price and change
        const price: number = Number(json.data.priceUsd)
        const change: number = Number(json.data.changePercent24Hr)

        // Get HTML IDs
        const idSym: HTMLInputElement = getId(obj.ids.sym)
        const idCh24: HTMLInputElement = getId(obj.ids.ch24)

        // Set Price
        idSym.innerHTML = price.toFixed(4) + " " + "USD"

        // Set Change and css color
        if (change < 0) {
            idCh24.style.color = "red"
            idCh24.innerHTML = change.toFixed(2) + " " + "%"
        }
        if (change >= 0) {
            idCh24.style.color = "green"
            idCh24.innerHTML = change.toFixed(2) + " " + "%"
        }

        // Return Promise
        return json

    } catch (err) {
        console.error(err)
    }
}

/** Get Stonk Price */
export const getStonkPrice = async (obj: SymbolInterface): Promise<stonk.StonkInterface> => {
    try {
        // Await response
        const response: Response = await fetch(obj.url)
        const json: stonk.StonkInterface = await response.json()

        // Parse json to price and change
        const price: number = json.c
        const change: number = json.dp

        // Get HTML IDs
        const idSym: HTMLInputElement = getId(obj.ids.sym)
        const idCh24: HTMLInputElement = getId(obj.ids.ch24)

        // Set Price
        idSym.innerHTML = price.toFixed(4) + " " + "USD"

        // Set Change and css color
        if (change < 0) {
            idCh24.style.color = "red"
            idCh24.innerHTML = change.toFixed(2) + " " + "%"
        }
        if (change >= 0) {
            idCh24.style.color = "green"
            idCh24.innerHTML = change.toFixed(2) + " " + "%"
        }

        // Return Promise
        return json

    } catch (err) {
        console.error(err)
    }
}
