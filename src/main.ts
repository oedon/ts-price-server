import * as coin from './coin.js'
import * as stonk from './stonk.js'
import * as call from './call.js'

const CoinLoopDaddy = async (): Promise<void> => {
    await call.getCoinPrice(coin.ada)
    await call.getCoinPrice(coin.btc)
    await call.getCoinPrice(coin.eth)
    await call.getCoinPrice(coin.xmr)
    setTimeout(CoinLoopDaddy, 30000)
}

const StonkLoopDaddy = async (): Promise<void> => {
    await call.getStonkPrice(stonk.appl)
    await call.getStonkPrice(stonk.amd)
    await call.getStonkPrice(stonk.pypl)
    await call.getStonkPrice(stonk.nflx)
    await call.getStonkPrice(stonk.nvda)
    await call.getStonkPrice(stonk.tsla)
    setTimeout(StonkLoopDaddy, 60000)
}

CoinLoopDaddy()
StonkLoopDaddy()
